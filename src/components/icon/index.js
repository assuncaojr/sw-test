import React from 'react'

const Icon = (props) => {
  const { path, alt, height } = props

  return (
    <img src={path} alt={alt} style={{height: height || 24}} />
  )
}

export default Icon
