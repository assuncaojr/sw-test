import React from 'react'
import './styles.scss'

export default class Footer extends React.PureComponent {
  render () {
    return (
      <footer>
        <div className='note'>
          The purpose of this project is just a use test of the React.JS Reactive Code Library. For non-commercial purposes, all rights to trademarks, images etc. belong to their respective owners.
          <br />
          Data source: SWApi, Wikipedia, and Google
        </div>
      </footer>
    )
  }
}