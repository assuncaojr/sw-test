import React from 'react'
import * as VARIABLES from '../../variables'
import './styles.scss'

const Header = () => (
  <header className='main-header'>
    <img src='../../../starwars_logo.png' className='main-logo image' alt={VARIABLES.SITENAME} />
    <div className='death-star'>
      <img src='../../../death_star.png' alt='Death Start' />
    </div>
  </header>
)

export default Header
