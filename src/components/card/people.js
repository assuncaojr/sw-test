import React from 'react'
import Icon from '../icon'
import * as Api from '../../services/Api'
import IconPlanet from '../../assets/images/ico_planet.png'
import IconSpecies from '../../assets/images/ico_species.png'
import IconGenderMale from '../../assets/images/ico_gender_male.png'
import IconGenderFemale from '../../assets/images/ico_gender_female.png'
import IconGenderUndefined from '../../assets/images/ico_gender_undefined.png'
import ImageNotFound from '../../assets/images/image_not_found.jpg'
import Preloader from '../../assets/images/preloader.svg'
import './styles.scss'

export default class CardPeople extends React.PureComponent {
  state = {
    loading: false,
    loadingImage: false,
    planet: null,
    species: null,
    images: null
  }

  async componentDidMount () {
    const { homeworld, species } = this.props.people
    await this.getCharacterImageData()
    await this.getPlanetName(homeworld)
    await this.getSpeciesName(species)
  }

  getPlanetName = async (url) => {
    const { name } = await Api.getPlanet(url.replace(/^\D+/g, ''));
    this.setState({ planet: name })
  }

  getSpeciesName = async (species) => {
    if ((!species) || (species && species.length <= 0)) {
      return
    }

    const { name } = await Api.getSpecies(species[0].replace(/^\D+/g, ''));
    this.setState({ species: name })
  }

  getCharacterImageData = async () => {
    try {
      this.setState({ loadingImage: true })
      const { name } = this.props.people
      const { value } = await Api.getCharacterAvatar(name)
      const images = value && value.length > 0 ? value[0]['thumbnailUrl'] : ImageNotFound

      this.setState({ loadingImage: false, images })
    } catch {
      this.setState({ loadingImage: false })
    }
  }

  getFilmsData = async () => {
    let { films } = this.props.people
    this.setState({ loading: true })
    let movies = []

    for (let i = 0; i < films.length; i++) {
      let { title, director, producer, release_date } = await Api.getFilm(films[i].replace(/^\D+/g, ''))
      movies.push({ title, director, producer, release_date })
    }

    this.setState({ loading: false })
    return movies
  }

  getStarshipData = async () => {
    let { starships } = this.props.people
    this.setState({ loading: true })
    let data = []

    for (let i = 0; i < starships.length; i++) {
      let { name, model, length, max_atmosphering_speed, starship_class } = await Api.getStarship(starships[i].replace(/^\D+/g, ''))
      data.push({ name, model, length, max_atmosphering_speed, starship_class })
    }

    this.setState({ loading: false })
    return data
  }

  getCharacterAvatarPathStyle = () => {
    let { loadingImage, images } = this.state
    try {
      if (loadingImage) {
        return {
          background: `url(${Preloader}) center center no-repeat`,
          backgroundSize: '10%',
          opacity: .7
        }
      }

      if ((!loadingImage && !images)) {
        images = ImageNotFound
      }

      return {
        background: `url(${images}) center center no-repeat`
      }
    } catch (error) {

      return {
        background: `url(${ImageNotFound}) center center no-repeat`
      }
    }
  }

  renderIconGender (type) {
    let data = {}
    switch (type) {
      case 'male':
        data.ico = IconGenderMale
        data.text = 'Male'
        break

      case 'female':
        data.ico = IconGenderFemale
        data.text = 'Female'
        break

      case 'n/a':
      default:
        data.ico = IconGenderUndefined
        data.text = 'Undefined'
    }

    return this.renderColIcoDesc(data)
  }

  renderColIcoDesc = (data) => (
    <React.Fragment>
      <Icon path={data.ico} alt={data.text} />
      <div className='is-size-6'>{ data.text ? data.text : 'Undefined'}</div>
    </React.Fragment>
  )

  detailCharacter = async () => {
    const films = await this.getFilmsData()
    const starships = await this.getStarshipData()

    const { callback, people } = this.props
    const { images, planet, species } = this.state
    return callback({ people, images, planet, species, films, starships})
  }

  render () {
    const { name, gender, films } = this.props.people
    const { planet, species, images, loading } = this.state
    const avatarStyles = this.getCharacterAvatarPathStyle(images)

    return (
      <div className='card card-people'>
        <div className='card-header has-text-centered'>
          <div className='card-image' style={avatarStyles}>
            <h2 className='is-size-2 is-size-3-mobile'>{name}</h2>
          </div>
        </div>

        <div className='card-content'>
          <div className='columns is-mobile card-icons'>
            <div className='column is-4 has-text-centered'>
              {this.renderColIcoDesc({ ico: IconPlanet, text: planet })}
            </div>

            <div className='column is-4 has-text-centered'>
              {this.renderIconGender(gender)}
            </div>

            <div className='column is-4 has-text-centered'>
              {this.renderColIcoDesc({ ico: IconSpecies, text: species })}
            </div>
          </div>
        </div>

        <div className='card-footer columns is-mobile'>
          <div className='column'>
            Film Participation: { films.length || 0 } 
          </div>

          <div className='column has-text-right'>
            <button 
              onClick={this.detailCharacter}
              className={loading ? 'is-loading button is-small is-warning' : 'button is-small is-warning'}>
              SEE MORE
            </button>
          </div>
        </div>
      </div>
    )
  }
}
