import axios from 'axios'
import * as VARIABLES from '../variables'

const getCharacters = async (page = 1) => {
  const response = await GetData(`people/?page=${page}`)
  return response.data
}

const getCharacter = async (id) => {
  const { data } = await GetData(`people/${id}`)
  return data
}

const getCharacterAvatar = async (name) => {
  try {
    const url = 'https://api.cognitive.microsoft.com/bing/v7.0/search';
    const headers = {
      'Ocp-Apim-Subscription-Key': `${VARIABLES.BINGKEY}\r\n`
    }

    const {data} = await axios({
      method: 'GET',
      headers,
      url: `${url}?q=${name}`
    })

    return data.images
  } catch(error) {
    throw error
  }
}

const getPlanet = async (id) => {
  const { data } = await GetData(`planets/${id}`)
  return data
}

const getSpecies = async (id) => {
  const { data } = await GetData(`species/${id}`)
  return data
}

const getFilm = async (id) => {
  const { data } = await GetData(`films/${id}`)
  return data
}

const getStarship = async (id) => {
  const { data } = await GetData(`starships/${id}`)
  return data
}

const GetData = async (url) => {
  try {
    const response = await axios({
      method: 'GET',
      url: `${VARIABLES.URLAPI}/${url}`
    })
    return response
  } catch (error) {
    throw error
  }
}

export { getCharacters, getCharacter, getCharacterAvatar, getPlanet, getSpecies, getFilm, getStarship }
