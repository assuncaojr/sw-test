import React from 'react'
import * as Api from '../../services/Api'
import { PopupboxManager, PopupboxContainer } from 'react-popupbox'
import CardPeople from '../../components/card/people'
import './styles.scss'
import 'react-popupbox/dist/react-popupbox.css'
import Preloader from '../../assets/images/preloader.svg'
import ImageNotFound from '../../assets/images/image_not_found.jpg'

export default class Home extends React.PureComponent {
  state = {
    hasMoreData: true,
    loadingMoreData: false,
    data: [],
    films: []
  }

  async componentDidMount () {
    const result = await this.get()
    this.setState({ data: result })
  }

  get = async (page = 1) => {
    return await Api.getCharacters(page)
  }

  loadMoreData = async () => {
    const { next } = this.state.data
    if (!next) {
      this.setState({ hasMoreData: false })
      return
    }

    const { data } = this.state
    this.setState({ loadingMoreData: true })

    const result = await this.get(next.replace(/^\D+/g, ''))
    const results = {
      next: result.next,
      results: [ ...data.results, ...result.results ], 
    }

    let hasMoreData = true;
    if (!result.next) {
      hasMoreData = false
    }

    this.setState({ loadingMoreData: false, hasMoreData, data: results })
  }

  detailCharacter = async (data) => {
    const { people, films, starships, images } = data
    const avatar = images || ImageNotFound

    const avatarStyles = {
      background: `url(${avatar}) center center no-repeat`
    }

    const content = (
      <div className='section section-content'>
        <div className='character-image' style={avatarStyles}>
          <h2
            className='font-title title is-size-1 is-size-3-mobile'>
            {people.name}
          </h2>
        </div>
        <div className='columns is-multiline'>
          <div className='column is-4'>
            <h3 className='font-subtitle subtitle is-size-4 is-size-6-mobile'>Bio</h3>
            {this.detailCharacterBioData(data)}
          </div>

          <div className='column is-4 films'>
            <h3 className='font-subtitle subtitle is-size-4 is-size-6-mobile'>Films</h3>
            { 
              films.map((film, key) => (
                <React.Fragment key={key}>
                  <div className='font-subtitle subtitle is-size-5'>{film.title}</div>
                  <div className='film-data'>
                    <strong>Director:</strong> {film.director}. 
                    <strong>Producter:</strong> {film.producer}. 
                    <strong>Release Date:</strong> {film.release_date}
                  </div>
                </React.Fragment>
              ))
            }
          </div>

          <div className='column is-4 films'>
            <h3 className='font-subtitle subtitle is-size-4 is-size-6-mobile'>Starships</h3>
            {starships && starships.length === 0 && <div className='notification'>:( Starship data not found</div>}
            { starships &&
              starships.map((starship, key) => (
                <React.Fragment key={key}>
                  <div className='film-data'>
                    <strong>Name:</strong> {starship.name}.&nbsp;
                    <strong>Model:</strong> {starship.model}.&nbsp;
                    <strong>Class:</strong> {starship.starship_class}.&nbsp;
                    <strong>Max Speed:</strong> {starship.max_atmosphering_speed}
                  </div>
                </React.Fragment>
              ))
            }
          </div>
        </div>
      </div>
    )

    PopupboxManager.open({
      content,
      config: {
        titleBar: {
          enable: true,
          text: 'Character Detail'
        },
        className: 'modal',
        fadeIn: true,
        fadeInSpeed: 500
      }
    })
  }

  detailCharacterBioData = (data) => {
    const { people, planet, species } = data

    return (
      <ul>
        <li><strong>Planet:</strong> {planet}</li>
        <li><strong>Species:</strong> {species}</li>
        <li><strong>Height:</strong> {people.height}</li>
        <li><strong>Hair Color:</strong> {people.hair_color}</li>
        <li><strong>Eye Color:</strong> {people.eye_color}</li>
        <li><strong>Skin Color:</strong> {people.skin_color}</li>
        <li><strong>Gender:</strong> {people.gender}</li>
      </ul>
    )
  }

  render () {
    const { results } = this.state.data
    const { hasMoreData, loadingMoreData } = this.state

    return (
      <div className='main'>
        <div className='container is-fluid characters'>
          <div className='box'>
            <h1>
              <span className='title font-title is-size-1 is-size-4-mobile'>Star Wars</span>
              <span className='subtitle font-subtitle is-size-2 is-size-5-mobile'> | Characters</span>
            </h1>

            <h4 className='subtitle font-subtitle is-size-5 is-size-6-mobile'>
              This list of characters from the Star Wars franchise contains only those <br />which are considered part of the official Star Wars canon.
              <br />
              <a href='https://en.wikipedia.org/wiki/List_of_Star_Wars_characters' target='_blank' rel="noopener noreferrer" className='link'>Learn more on Wikipedia</a>
            </h4>
          </div>

          <div className='columns is-multiline'>
            {!results &&
            <div className='column is-12 has-text-centered'>
              <div><img src={Preloader} alt='Carregando Dados' /></div>
            </div>
            }

            {results && results.length > 0 &&
              results.map((people, key) => (
                <div key={key} className='column is-3'>
                  <CardPeople people={people} callback={this.detailCharacter} className='column is-3'>{people.name}</CardPeople>
                </div>
              ))
            }

            {results && results.length > 0 &&
              <div className='column is-12'>
                <div className={hasMoreData ? 'load-more has-text-centered' : 'is-hidden'}>
                  <button
                    onClick={this.loadMoreData}
                    className={loadingMoreData ? 'is-loading button is-large is-black' : 'button is-large is-black'}>
                    Load more characters
                  </button>
                </div>
              </div>
            }
          </div>

        </div>

        <PopupboxContainer />
      </div>
    )
  }
}
