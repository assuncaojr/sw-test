import React from 'react';
import './assets/scss/index.scss'
import Header from './components/header'
import Footer from './components/footer'
import Home from './views/home'

function App() {
  return (
    <React.Fragment>
      <Header />
      <Home />
      <Footer />
    </React.Fragment>
  );
}

export default App;
