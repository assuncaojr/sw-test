This project was developed using the React.JS javascript library for the purpose of reading and detailing all characters from the Star Wars film franchise. For data collection, we used the public API swapi.com and Bing Images.

In this project we used the Bing Images API to display profile images of Saga characters. For this a free Key has been created with 7 days validity. If necessary a new key can be created at: [https://azure.microsoft.com/en-us/services/cognitive-services/bing-image-search-api/]

After the access key has been specified the 
### /src/variables.js 
file must be updated

## If needed, install React JS.
[https://reactjs.org]


## Available Scripts
In the project directory, add packages depedencies:
### `npm install` or `yarn install`

Runs the app in the development mode.<br>
### `npm start` or `yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
